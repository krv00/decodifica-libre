# Decodifica Libre

Programa para decodificar libros digitales de buscalibre.com

## ¿Por qué Buscalibre codifica sus libros digitales?

Buscalibre vende libros digitales como "epub", Sin embargo no es cierto. Son libros solo accesibles desde su sitio web. Si se intenta copiar o descargar el libro, el texto obtenido es ilegible como este:

**SO egNqO ALIdAbI7 gb LgbqO zAb VAt7Oz**

Buscalibre codifica sus libros para que solo puedan ser leídos desde su página web y así no puedan ser compartidos.

## Decodificar los libros

Los libros digitales de buscalibre tienen un cifrado sencillo: las letras minúsculas, mayúsculas y los números se reemplazan por otro signo alfanumérico elegido, al parecer, aleatoriamente. Similar al cifrado Cesar pero con arbitrariedad en vez de rotación.

Si se aplica la decodificación, se puede recuperar el texto original:

**No puedo imaginar un mundo sin libros**

## Uso

*Se requiere Python 3*

El texto codificado, descargado de buscalibre, debe agregarse en el archivo [entrada.txt](https://gitlab.com/krv00/decodifica-libre/-/blob/main/entrada.txt). El decodificador funciona ejecutando el script [Decodificalibre.py](https://gitlab.com/krv00/decodifica-libre/-/blob/main/Decodificalibre.py). Luego de eso, el texto decodificado se encontrará en el archivo [salida.txt](https://gitlab.com/krv00/decodifica-libre/-/blob/main/salida.txt).

Con el texto en claro, se puede leer sin inconvenientes.

[Aquí](https://juanjobote.com/calibre-tutorial-ebook-libros-electronicos/) hay una guía para elaborar un libro digital (epub u otro formato) con Calibre.

## Clave

La clave utilizada para decodificar se encuentra en [clave.ods](https://gitlab.com/krv00/decodifica-libre/-/blob/main/clave.ods). Es posible que buscalibre puedan codificar distintos libros con claves distintas. Si el decodificador no funciona, es recomendable intentar reconstruir la clave, comparando el texto legible con el codificado. 

En Decodificalibre.py, el objeto o diccionario **decoder** contiene la clave para hacer funcionar el programa.

## Licencia

[Licencia Pública General GNU](https://www.gnu.org/licenses/gpl-3.0.html)