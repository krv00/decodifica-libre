#DecodificaLibre

# importar módulo
import re
texto = []

# función decodificar
def decodificar(y):
    y = y.replace('\n', '')
    cadena = y
    decoder = {
        "I" : "a", "t" : "b", "K" : "c", "q" : "d", "N" : "e", "X" : "f", "d" : "g", "y" : "h", "A" : "i", "D" : "j", "c" : "k", "V" : "l", "L" : "m", "b" : "n", "O" : "o", "e" : "p", "4" : "q", "7" : "r", "z" : "s", "F" : "t", "g" : "u", "h" : "v", "o" : "w", "j" : "x", "i" : "y", "x" : "z",
        "f" : "A", "U" : "B", "m" : "C", "G" : "D", "Q" : "E", "M" : "F", "6" : "G", "k" : "H", "1" : "I", "Y" : "J", "2" : "K", "H" : "L", "8" : "M", "S" : "N", "B" : "O", "P" : "P", "n" : "Q", "p" : "R", "E" : "S", "l" : "T", "a" : "U", "C" : "V", "5" : "W", "v" : "X", "u" : "Y", "r" : "Z",
        "9" : "0", "s" : "1", "J" : "2", "T" : "3", "Z" : "4", "R" : "5", "3" : "6", "w" : "7", "0" : "8", "W" : "9"
        }

    # Declaración de expresión regular para buscar coincidencias con el diccionario
    regex = re.compile("(%s)" % "|".join(map(re.escape, decoder.keys())))
    # Decodificación
    nueva_linea = regex.sub(lambda x: str(decoder[x.string[x.start() :x.end()]]), cadena)
    # Guardar línea en este objeto llamado texto
    texto.append(nueva_linea)    

# Abre archivo con texto codificado    
with open('entrada.txt', encoding='utf-8') as archivo:
    # envía línea por línea para que la función "decodificar" transforme el texto
    for linea in archivo:
        decodificar(linea)

# Abre archivo para guardar nuevo texto decodificado   
with open('salida.txt', 'w', encoding='utf-8') as f:
    f.write('\n'.join(texto))
print("\n ¡LISTO! \n")
    

# Ejemplo
#“SO egNqO ALIdAbI7 gb LgbqO zAb VAt7Oz”.
#"No puedo imaginar un mundo sin libros".
 
